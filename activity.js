// CREATE
    // allows us to create documents under a collection or create a collection if it does not exist yet.

    // Syntax
    // db.collections.insertOne()
         // allows us to insert or create one document
         // collections -  stands for collection name

    db.hotel.insertOne(
    {
    	"name": "single",
    	"accomodates": 2,
    	"price": 1000,
    	"description": "A simple room with all the basic neecessities",
    	"rooms_available": 10,
    	"isAvailable": false

    }

  )

    // db.collections.insertMany()
        // allows us to insert or create two or more documents.

    db.hotel.insertMany([
    {

        "name": "double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for a small family going on a vacation",
        "rooms_available": 5,
        "isAvailable": false

    },
    {

        
        "name": "queen",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": false
    },


])

// READ

// Syntax:
   // db.collections.findOne({criteria: value})
       // allows us to find the document that matches our criteria

   db.hotel.find({"name" : "double"});

// UPDATE
   // allows us to update documents.
   // this  also use criteria or filter.
   // $set operator
   // REMINDER: Updates are permanent and can't be rolled back.

// Syntax:
   // db.collections.updateOne({criteria: value},{$set: {}})
       // allows us to update one document that satisfy a criteria


db.hotel.updateOne({"name" : "queen"}, {$set:{"rooms_available" : 0}});

//DELETE
  // allows us to delete documents
  // provide criteria or filters to specify which document to delete form the collection.
  // REMINDER: Be careful when deleting documents, because it will be complicated to retrieve them back again. 

//Syntax:
  // db.collections.deleteMany({criteria: value})
     // allows us to delete all items that macthes our criteria.

db.hotel.deleteMany({"rooms_available" : 0});





